﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointTracker : MonoBehaviour
{
    private Vector3[] waypointList = null;
    public AirplaneAiControl aiController;
    private Vector3 targetPosition;
    public float switchWaypointDistance;
    private int index;


    void OnStart()
    {
        
        aiController = GetComponent<AirplaneAiControl>();
        Debug.Log("OnStart: Waypoint Tracker");
    }


    // Update is called once per frame
    void FixedUpdate()
    {

        if (waypointList != null)
        {
            
            Debug.DrawRay(transform.position, targetPosition);
            float distanceToTarget = Vector3.Distance(targetPosition, transform.position);
            
            if (distanceToTarget < switchWaypointDistance)
            {
                if (index < waypointList.Length)
                    NextTarget();
                else
                {
                    waypointList = null;
                    aiController.SetAutoPilot(true);
                    return;
                }
            }

            //aiController.Move(targetPosition);
        }

    }

    public void Init(Vector3 position)
    {
       /* waypointList = new Vector3[1];
        waypointList[0] = position;
        targetPosition = position;
        Debug.Log("Target Position:" + targetPosition);
    */
    }

    public void SetWaypointList(Vector3[] waypoints)
    {
        aiController.SetAutoPilot(false);
        waypointList = waypoints;
        index = 0;
        NextTarget();
    }

    private void NextTarget()
    {
        targetPosition = waypointList[index++];
    }
}
