﻿using UnityEngine;
using System;
using TMPro;

public class TimeManager : MonoBehaviour
{
    public TextMeshProUGUI timerText;


    [Header("Time Values")]
    [Range(0, 60)]
    public int seconds;
    [Range(0,60)]
    public int minutes;
    [Range(0,23)]
    public int hours;

    public Color fontColor;

    private float currentSeconds;
    private int timerDefault;
    private bool start = false;
    private int currentTime;



    // Start is called before the first frame update
    void Start()
    {
        SimManager._instance._timer = this;
        /*timerText.color = fontColor;
        timerDefault = 0;
        timerDefault += (seconds + (minutes * 60) + (hours * 60 * 60));
        currentSeconds = timerDefault;
        timerText.text = TimeSpan.FromSeconds(currentSeconds).ToString(@"hh\:mm\:ss");
        */

    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            currentSeconds += Time.deltaTime;
            timerText.text = TimeSpan.FromSeconds(currentSeconds).ToString(@"hh\:mm\:ss");        
            //Debug.Log((int)currentSeconds);

            /*
            if (SimManager._instance.spawnTime == (int)currentSeconds)
            {
                
                SimManager._instance.ActivateAircraft();

            }*/

        }

    }

    public int CurrentSeconds
    {
        get { return (int)currentSeconds;}
    }


    public void StartTime()
    {
        start = true;
        currentSeconds += Time.deltaTime;
        SimManager._instance.RunExercise();
    }


    public void SetTime(int hours, int minutes)
    {
        timerDefault += (minutes * 60) + (hours * 60 * 60);
        currentSeconds = timerDefault;
        timerText.text = TimeSpan.FromSeconds(currentSeconds).ToString(@"hh\:mm\:ss");
    }




}
