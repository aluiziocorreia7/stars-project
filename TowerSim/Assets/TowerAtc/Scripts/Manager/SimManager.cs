﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using IndiePixel;
using System.Collections;

public class SimManager : MonoBehaviour{

    public static SimManager _instance;
    private bool _isLoaded = false;
    private string _airportCode;
    private Dictionary<string, Transform> _stands;
    private List<FlightPlanData> _flightPlans;
    private Dictionary<string, AirplaneAiControl> _aircraftsAIControllers;
    private List<Transform> waitingForActivation = new List<Transform>(); 

    //Store the data after an exercises is loaded
    private Exercise _currentExercise;


    [HideInInspector]  public Spawner _networkSpawner;
    [HideInInspector] public TimeManager _timer;
    public Transform stands;
    public GameObject aircraftPrefab;
    public Transform arrivalPosition;
    public Transform target;

    public int spawnTime;
    private Transform aircraftOnHold;



    public Exercise CurrentExercise
    {
        get { return this._currentExercise; }
        set { this._currentExercise = value; }

    }

    private void Awake()
    {
        if (_instance == null) { 
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }


    public void LoadExercise(Exercise exercise)
    {
        _currentExercise = exercise;
        _flightPlans = _currentExercise.FlightPlans;
        _airportCode = _currentExercise.AirportCode;
        Setup();
        //SceneManager.LoadScene(_airportCode);
        _isLoaded = true;

        //Debug.Log("Exercise: " + _currentExercise.Name + " is Loaded");
    }


    public void Setup()
    {
        _timer.SetTime(_currentExercise.Hours, _currentExercise.Minutes);
        LoadStands();
        //LoadTaxiways();
        //LoadSid();
        LoadAircrafts();
        Debug.Log("Setup all the configurations");
    }


    private void LoadStands()
    {
        if(stands!= null)
        { 
        _stands = new Dictionary<string, Transform>();

            foreach (Transform stand in stands)
            {
                _stands.Add(stand.name, stand);
               // Debug.Log("Stand: " + _stands[stand.name].transform.name);
            }

        }
        else
        {
           Debug.Log("Stand: não existe");
        }
    }



    //Spawn the aircrats from the flight plans list
    private void LoadAircrafts()
    {
        _aircraftsAIControllers = new Dictionary<string, AirplaneAiControl>();
        
        //For each flight plan , spawns the aircraft 
        foreach (FlightPlanData flpData in _flightPlans)
        {
            flpData.SetTimeInSeconds();          
            Transform aircraft = null;
            if (flpData.Origin == _airportCode)
            {
                Transform stand = _stands["Stand " + flpData.Stand];
                
                if (stand != null)
                {   aircraft = _networkSpawner.SpawnAircraftOnStand(aircraftPrefab, stand, flpData);
                    aircraft.name = flpData.CallSign;
                    aircraft.GetComponent<FlightPlan>().Setup(flpData, _airportCode);
                    aircraft.GetComponent<Animator>().SetTrigger("stand");
                }
            }
            else
            {
                //Decidir como obter o "spawn position" para uma aeronave em voo
                //aircraftOnHold = _networkSpawner.SpawnAircraftOnFlight(aircraftPrefab,arrivalPosition,flpData);
                
                aircraftOnHold = Instantiate(aircraftPrefab, arrivalPosition.position, arrivalPosition.rotation).transform;
                aircraftOnHold.name = flpData.CallSign;
                aircraftOnHold.GetComponent<FlightPlan>().Setup(flpData, _airportCode);
                aircraftOnHold.gameObject.SetActive(false);
                waitingForActivation.Add(aircraftOnHold);
                
                //Debug.Log("SpawnTime :" + spawnTime);
            }
        }
    }


    public IEnumerator ActivateAircraft(Transform aircraftOnHold)
    {
       
        //Transform aircraftOnHold = waitingForActivation[0];
        float spawnTime = aircraftOnHold.GetComponent<FlightPlan>().TimeInSeconds;
        Debug.Log("AIRCRAFT " + aircraftOnHold.name + " WAITING....");

        int wait = ((int)spawnTime - (int)_timer.CurrentSeconds);
        Debug.Log("WAITING....: "+ wait);

        yield return new WaitUntil(() =>  (int) _timer.CurrentSeconds == (int)spawnTime);

            aircraftOnHold.gameObject.SetActive(true);
            _networkSpawner.SpawnAircraftOnFlight(aircraftOnHold.gameObject, arrivalPosition);
            aircraftOnHold.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 40f);
            aircraftOnHold.GetComponent<AirplaneAiControl>().EnableTakeOff();
            aircraftOnHold.GetComponent<AirplaneAiControl>().SetAutoPilot(true);
            aircraftOnHold.GetComponent<Animator>().SetTrigger("fly");
           
                    
            StopCoroutine("ActivateAircraft");
            Debug.Log("AIRCRAFT ACTIVATED: " + aircraftOnHold.name);
    }


    public void RunExercise()
    {
        //Start the clock time

        foreach (Transform aircraft in waitingForActivation)
        {
            StartCoroutine(ActivateAircraft(aircraft));
        }

    }





}
