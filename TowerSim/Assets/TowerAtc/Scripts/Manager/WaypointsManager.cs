﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class WaypointsManager : MonoBehaviour
{
    private Transform[] taxiways;
    [SerializeField]
    private GameObject waypointPrefab;
    private List<TaxiPath> taxiPathList = new List<TaxiPath>();

    private class TaxiPath
    {
        public string _callsign;
        public List<string> _taxiways;
        public Dictionary<string, Vector3[]> _waypoints;

        public TaxiPath(string callsign, List<string> taxiways , Dictionary<string, Vector3[]> waypoints)
        {
            _callsign = callsign;
            _taxiways = taxiways;
            _waypoints = waypoints;
        }
    }



    private void Awake()
    {
        taxiways = transform.GetComponentsInChildren<Transform>();

    }


    public void SetVisibleTaxiways(string callsign,bool value)
    {
        List<string> list = GetTaxiways(callsign);
        if (list != null)
        {

            foreach (var taxiway in taxiways)
            {
                if (list.Contains(taxiway.name))
                {
                    taxiway.GetComponent<LineRenderer>().enabled = value;
                }

            }

        }
    }



    /******
     * Funçoes para criar um taxipath automatico de acordo com os 
     * taxiways predefinidos no sistema
     * ***/


    //Print de um taxi
    private void PrintTaxiPath(string callsign)
    {
        foreach (var item in GetTaxiPathByCallsign(callsign)._taxiways)
        {
            Debug.Log("Callsign: " + callsign + " and taxiway " + item);
        }
    }

    //Retorna uma Taxipath se encontrar o Callsign, caso contrario retorn null.
    private TaxiPath GetTaxiPathByCallsign(string callsign)
    {
      

        foreach (var item in taxiPathList)
        {
            if(item._callsign == callsign)
            {
                return item;
            }
        }

        return null;
    }

    //Actualiza a lista que contem todos os taxipath agrupadas por callsign.
    private void UpdateTaxiPathList(TaxiPath taxiPath)
    {

        foreach (var item in taxiPathList)
        {
            if (item._callsign == taxiPath._callsign)
            {
                taxiPathList.Remove(item);
                taxiPathList.Add(taxiPath);
                return;
            }
        }

    }

    //Cria uma nova taxiPath associada a um callsign novo.
    public void CreatePath(string callsign, string taxiway, Vector3[] path)
    {
        TaxiPath taxiPath = GetTaxiPathByCallsign(callsign);
        if (taxiPath != null)
        {
            if (!taxiPath._waypoints.ContainsKey(taxiway))
            {
                taxiPath._waypoints.Add(taxiway, path);
                taxiPath._taxiways.Add(taxiway);
                UpdateTaxiPathList(taxiPath);
                Debug.Log(" TaxiPath actualizado: ");
            }
        }
        else
        {
            Dictionary<string, Vector3[]> waypoints = new Dictionary<string, Vector3[]>();
            List<string> taxiways = new List<string>();
            taxiways.Add(taxiway);
            waypoints.Add(taxiway, path);
            taxiPathList.Add(new TaxiPath(callsign, taxiways, waypoints));
            Debug.Log("New Path created:");
        }

        PrintTaxiPath(callsign);

    }

    //Remove um taxiway da taxiPath e atualiza a lista TaxiPathList
    public void RemoveTaxiwayFromPath(string callsign, string taxiway)
    {
        TaxiPath taxiPath = GetTaxiPathByCallsign(callsign);
        if(taxiPath != null)
        {
            taxiPath._taxiways.Remove(taxiway);
            taxiPath._waypoints.Remove(taxiway);
            UpdateTaxiPathList(taxiPath);
            Debug.Log("Remove taxiway from path");
        }

        PrintTaxiPath(callsign);
    }

    //Remove uma  taxiPath da lista TaxiPathList de acordo com um callsign
    public void RemovePathByCallsign(string callsign)
    {
        taxiPathList.Remove(GetTaxiPathByCallsign(callsign));
        Debug.Log("Remove TaxiPath from list");

    }


    //Retorna os taxiways previamente selecionados para um callsign
    public List<string> GetTaxiways(string callsign)
    {
        TaxiPath taxiPath  = GetTaxiPathByCallsign(callsign);
        if(taxiPath != null)
        {
            return taxiPath._taxiways;
        }

        return null;
    }


    //Remove um taxiPath da taxiPathList, de acordo com um calsign escolhido
    public void RemoveTaxiPath(string callsign)
    {
        TaxiPath taxiPath = GetTaxiPathByCallsign(callsign);

        if (taxiPath != null)
            taxiPathList.Remove(taxiPath);
    }

    //Retorna a lista com todos os waypoints de cada taxiways incluidos no taxipath da aeronave
    public Vector3[] GetTaxiwaysNodes(string selectedCallsign,Transform aircraft)
    {
        TaxiPath taxiPath = GetTaxiPathByCallsign(selectedCallsign);
       
        if(taxiPath!= null)  
        {
            int size = 0;
            foreach (var item in taxiPath._taxiways)
            {

                size += taxiPath._waypoints[item].Length;
            }

            Vector3[] waypointList = new Vector3[size];
            waypointList = OrganizeNodesByDistance(aircraft.position, taxiPath, waypointList);

            return waypointList;
        }

        return null;
    }


    //Organiza os waypoints de acordo com a distancia de cada taxiway no path da aeronave.
    private Vector3[] OrganizeNodesByDistance(Vector3 initialPosition, TaxiPath taxiPath, Vector3[] waypointList)
    {
        int index = 0;
        foreach (var taxiway in taxiPath._taxiways)
          {
            Vector3[] waypoints = taxiPath._waypoints[taxiway];
            Vector3 firstWaypoint = waypoints[0], lastWaypoint = waypoints[waypoints.Length - 1];
            float distance1 = (firstWaypoint - initialPosition).magnitude;
            float distance2 = (lastWaypoint - initialPosition).magnitude;
            

            if (distance1 < distance2)
            {   
                foreach (var waypoint in waypoints)
                {
                    waypointList[index++] = waypoint;
                }

                initialPosition = lastWaypoint;

            }
            else
            {           //Reverse the nodes
                for (int i = waypoints.Length-1; i >= 0 ; i--)
                {
                    waypointList[index++] = waypoints[i];
                }

                initialPosition = firstWaypoint;
            }

            Debug.Log("Taxiway: " +taxiway + "----WaypointList: "+waypointList);
            }
        return waypointList;
       
    }






    /*********
     * END
     ********/



    public void SpawnWaypoint(Vector3 waypointPosition,string callsign)
    {
        GameObject waypoint = Instantiate(waypointPrefab, waypointPosition, Quaternion.identity) as GameObject;
        Transform pathByName = FindWaypointsByCallsign(callsign);
        waypoint.transform.SetParent(pathByName);
        pathByName.SetParent(this.transform);
    }


    private Transform FindWaypointsByCallsign(string callsign)
    {
        Transform node = transform.Find(callsign);

        if (node == null)
        {
             GameObject newNode = new GameObject();
             newNode = Instantiate(newNode,Vector3.zero, Quaternion.identity) as GameObject;
             newNode.name = callsign;
             return newNode.transform;
        }
        else
        {
            return node;
        }
       
    }

   
}
