﻿using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using TMPro;

public  class XMLManager : MonoBehaviour
{

    public static XMLManager instance;
    //Load Exercise Input Field
    [SerializeField] private TMP_InputField _fileName;

    private void Awake()
    {
        instance = this;
       
    }


    //Save the Exercise data into XML file
    public void SaveExerciseData(Exercise data)
    { 
        XmlSerializer serializer = new XmlSerializer(typeof(Exercise));

        FileStream stream = new FileStream(Application.dataPath + "/Resources/" + data.Name + ".xml", FileMode.Create);
        //FileStream stream = new FileStream(Application.persistentDataPath+ "/Exercises/" + data.Name + ".xml", FileMode.Create);
        serializer.Serialize(stream, data);
        stream.Close();

        Debug.Log("Exercise: " + data.Name + " saved.");
    }


    //Load the Exercise data from a XML file
    public void LoadExerciseData()
    {
       
        XmlSerializer serializer = new XmlSerializer(typeof(Exercise));
        FileStream stream = new FileStream(Application.dataPath + "/Resources/" + _fileName.text + ".xml", FileMode.Open);
        //FileStream stream = new FileStream(Application.persistentDataPath + "/Exercises/" + name + ".xml", FileMode.Open);
        Exercise exercise = (Exercise) serializer.Deserialize(stream);

        Debug.Log("Exercise: " + exercise.Name + " data is loaded from the XML file.");

        //Load Exercise
        SimManager._instance.LoadExercise(exercise);
    }





}
