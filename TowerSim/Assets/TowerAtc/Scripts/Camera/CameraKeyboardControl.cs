﻿using UnityEngine;
using System.Collections;

public class CameraKeyboardControl : MonoBehaviour {

	public int zoom = 10;
	public int normal =  77;
	public float smooth = 3f;
	public float turnSpeed = 60f;


	private float turnInputHorizontal,turnInputVertical;
	private string turnAxisHorizontal;
	private string turnAxisVertical;
	private Rigidbody rigidbody;

	private bool isZoomed = false;
	 

	private void Awake()
	{
		rigidbody = GetComponent<Rigidbody> ();
	}

	private void Start()
	{
		turnAxisHorizontal = "Horizontal";
		turnAxisVertical = "Vertical";
		turnInputHorizontal = 0f;
		turnInputVertical = 0f;


	}

	// Update is called once per frame
	void Update () 
	{

		turnInputHorizontal = Input.GetAxis (turnAxisHorizontal);
		turnInputVertical = Input.GetAxis (turnAxisVertical);

		//Camera Movement
		if (Mathf.Abs(turnInputHorizontal) >= 0.1f) 
		{	//turn Horizontal

			float turnAmount = turnInputHorizontal * turnSpeed * Time.deltaTime;
			Quaternion turnRotation = Quaternion.Euler (0f, turnAmount, 0f);
			rigidbody.MoveRotation(rigidbody.rotation * turnRotation);
		
		}

		//Camera Movement
		if (Mathf.Abs(turnInputVertical) >= 0.1f) 
		{	//Turn Vertical

			float turnAmount = turnInputVertical * turnSpeed * Time.deltaTime;
			Quaternion turnRotation = Quaternion.Euler (- turnAmount,0f, 0f);
			rigidbody.MoveRotation(rigidbody.rotation * turnRotation);

		}


		//ZoomIn and ZoomOut
		if(Input.GetMouseButtonDown(1))
			isZoomed = !isZoomed;


		if(isZoomed)
		{
				GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, zoom, Time.deltaTime*smooth);

		}
		else {
			GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, normal, Time.deltaTime*smooth);
			
			}




	}
}
