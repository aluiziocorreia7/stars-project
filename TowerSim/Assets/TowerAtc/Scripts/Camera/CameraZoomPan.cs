﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomPan : MonoBehaviour
{

    public float panSpeed = 600f;
    public float panBorderThickness = 10f;
    public Vector2 panLimit;                   //Limits for the 2D Pan (x,y)

    public float scrollSpeed = 20f;

    public Camera camOrtographic;
    public float minSize = 150f, maxSize = 1900f;




    // Update is called once per frame
    void Update()
    {
        //Camera Position
        Vector3 pos = transform.position;

        //Move camera Up  || Input.mousePosition.y >= Screen.height - panBorderThickness
        if (Input.GetKey("w") )
        {
            pos.x -= panSpeed * Time.deltaTime;
        }

        //Move camera Down || Input.mousePosition.y <= panBorderThickness
        if (Input.GetKey("s") )
        {

            pos.x += panSpeed * Time.deltaTime;
        }


        //Move camera Left || Input.mousePosition.x <= panBorderThickness
        if (Input.GetKey("a") )
            {

                pos.z -= panSpeed * Time.deltaTime;
            }

        //Move camera Right  || Input.mousePosition.x >= Screen.width - panBorderThickness
        if (Input.GetKey("d") )
            {

                pos.z += panSpeed * Time.deltaTime;
            }
      

        //Handles the Zoom
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        float zoom =  scroll * scrollSpeed * 10f * Time.deltaTime;
        zoom = camOrtographic.orthographicSize - zoom;


        //Clamp the camera's Pan and Zoom within the limits
        pos.x = Mathf.Clamp(pos.x, -panLimit.y, panLimit.y);
        pos.z = Mathf.Clamp(pos.z, -panLimit.x, panLimit.x);
        zoom = Mathf.Clamp(zoom, minSize, maxSize);

        //Update the pan and zoom
        transform.position = pos;
        camOrtographic.orthographicSize = zoom;

        

    }
}
