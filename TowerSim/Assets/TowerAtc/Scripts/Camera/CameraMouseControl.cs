﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouseControl : MonoBehaviour
{
    [Header("Camera Properties")]
    public bool pauseMouse = true;
    public float mouseSpeed = 1.0f;

    public bool lockcamera = false;
    

    public float zoomSpeed= 10f;

    private float mouseX = -90.0f , mouseY = 0f;

    // Start is called before the first frame update
    void Start()
    {
        if (!pauseMouse)
        {
            return;
        }
        
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Locked;

    }

    // Update is called once per frame
    void Update()
    {
        if (!lockcamera) { 
             RotateCamera();
             ZoomCamera();
        }
       
    }



    private void RotateCamera()
    {
        mouseX += Input.GetAxis("Mouse X");
        mouseY -= Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(mouseY, mouseX, 0f);
    }


    private void ZoomCamera()
    {
        zoomSpeed = GetComponent<Camera>().fieldOfView;
        float scroll = Input.GetAxis("Mouse ScrollWheel");

        zoomSpeed = scroll * zoomSpeed * 100f * Time.deltaTime;


        GetComponent<Camera>().fieldOfView -= zoomSpeed;

    }

}
