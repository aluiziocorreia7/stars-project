﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPathDesigner : MonoBehaviour
{
    private LineRenderer lr;
    [SerializeField]private Transform[] points;
    private List<Transform> nodes = new List<Transform>();

    public Color lineColor;

    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
      

    }

    private void Start()
    {
        SetUpLine();
    }


    public void SetUpLine()
    {
        lr.positionCount = points.Length;

        for (int i = 0; i < points.Length; i++)
        {
            lr.SetPosition(i, points[i].position);
        }

    }


    private void OnDrawGizmos()
    {
        Gizmos.color = lineColor;
        Transform[] pathTransforms = GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for(int i= 0; i < pathTransforms.Length; i++)
        {
            if(pathTransforms[i] != transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }

        for(int i = 0; i < nodes.Count; i++)
        {
            Vector3 currentNode = nodes[i].position;
            Vector3 previousNode = Vector3.zero;

            if (nodes.Count > 1 && i > 0)
            {
                previousNode = nodes[i - 1].position;
                Gizmos.DrawLine(previousNode, currentNode);
            }

            
            Gizmos.DrawSphere(currentNode, 2f);
        }

    }

}
