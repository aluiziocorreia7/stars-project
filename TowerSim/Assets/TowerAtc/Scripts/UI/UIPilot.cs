﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIPilot : MonoBehaviour
{
    public Transform airplane;
    public TMP_Text altitudeTxt;
    public TMP_Text callsignTxt;

    public Button taxiButton;
    public Button takeOffButton;
    public Button holdPositionButton;
    public Button continueTaxiButton;
    public Button pushAndStartButton;


    public AirplaneAiControl aiControl;
    public int currentAltitude;
    public float speedInput;
    public float maxAltitude = 5000;
    const float feetToMeters = 0.3048f;

    // Start is called before the first frame update
    void Awake ()
    {
        //aiControl = airplane.GetComponent<AirplaneAiControl>();
        currentAltitude = int.Parse(altitudeTxt.text);
        
    }

    // Update is called once per frame
    void Update()
    {
       
        /* 
        //Get Right MouseClick from LOCAL PILOT 
        if (Input.GetMouseButtonDown(1))
        {
            
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            { 
                //Convert fts to meters
                float altitudeMeters = currentAltitude * feetToMeters;  
                Vector3 position = new Vector3(hit.point.x, altitudeMeters, hit.point.z);
                

                //Set the waypoint target 
                aiControl.SetTarget(position);
                
            }
        }*/

    }

    //Add 500ft to the current altitude
    public void Climb()
    {
     
        currentAltitude += 500;
        currentAltitude =(int) Mathf.Clamp(currentAltitude, 0, maxAltitude);
        altitudeTxt.text = currentAltitude.ToString();
        //aiControl.ChangeAltitude(currentAltitude * feetToMeters);
    }                                     

    //Subtract 500ft to the current altitude
    public void Descend()
    {
        currentAltitude -= 500;
        altitudeTxt.text = currentAltitude.ToString();
        //aiControl.ChangeAltitude(currentAltitude * feetToMeters);

    }

    public void ChangeSpeed(float amount)
    {
        speedInput = amount;
        Debug.Log("UI Pilot--Speed input:" + speedInput);

    }

   

    public void Callsign(string callsign)
    {
        callsignTxt.text = callsign;
    }

}
