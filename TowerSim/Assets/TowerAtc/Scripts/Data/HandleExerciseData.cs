﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HandleExerciseData : MonoBehaviour
{
   
    //Exercise Input Fields
    public InputField _name;
    public InputField _airportCode;
    public InputField _hours;
    public InputField _minutes;

    //Load Exercise Input Field
    public TMP_InputField _fileName;
   

    private Exercise exercise;
    private List<FlightPlanData> flightPlans = new List<FlightPlanData>();


    //Flight Plan InputField
    public InputField _callsign;
    public InputField _time;
    public InputField _origin;
    public InputField _destination;
    public InputField _type;
    public InputField _registration;
    public InputField _route;
    public InputField _stand;

   

    //When the user clicks the SAVE button, 
    //saves all the data from an exercise
    public void NewExerciseInput()
    {

        //Get the exercise input data from the input fields
        exercise = new Exercise();
        exercise.Name = _name.text;
        exercise.AirportCode = _airportCode.text;
        exercise.Hours = int.Parse(_hours.text);
        exercise.Minutes = int.Parse(_minutes.text);

        Debug.Log("Name: " + _name.text + "  Time: " + _hours.text + ":" + _minutes.text);

        //Only saves Exercises with flight plans created.
        if (flightPlans.Count > 0)
        {
            exercise.FlightPlans = flightPlans;
            //Save the exercise
            XMLManager.instance.SaveExerciseData(exercise);

            flightPlans.Clear();

            //Display a succesfull message to the user
        }
        else
        {
            Debug.LogError("You must create at least one flight plan");
        }

       


    }


    //When the user clicks the Add button, gets the flight plan input data
    // store it in a list of FlighPlans
    public void NewFlightPlanInput()
    {
        //Get fligt plan data from the input fields
        FlightPlanData flightPlan = new FlightPlanData();

        flightPlan.CallSign = _callsign.text;
        flightPlan.Time = _time.text;
        flightPlan.Origin = _origin.text;
        flightPlan.Destination = _destination.text;
        flightPlan.Type = _type.text;
        flightPlan.Registration = _registration.text;
        flightPlan.Route = _route.text;
        flightPlan.Stand = _stand.text;

        flightPlan.SetTimeInSeconds();

        //Insert into a list of flight plans
        flightPlans.Add(flightPlan);

        /*
        Debug.Log(flightPlan.CallSign);
        Debug.Log(flightPlan.Time);
        Debug.Log(flightPlan.Origin);
        Debug.Log(flightPlan.Destination);
        Debug.Log(flightPlan.Type);
        Debug.Log(flightPlan.Registration);
        Debug.Log(flightPlan.Route);
        Debug.Log(flightPlan.Stand);
        */

        //Empty all the input fields
         _callsign.text = "";
         _time.text = "";
        _origin.text = "";
        _destination.text = "";
        _type.text = "";
        _registration.text = "";
        _route.text = "";
        _stand.text = "";
        
    
}

    //When the user clicks the load exercise button
    public void LoadFileName()
    {
        //Debug.Log(_fileName.text);

        //XMLManager.instance.LoadExerciseData(_fileName.text);

    }

    

}
