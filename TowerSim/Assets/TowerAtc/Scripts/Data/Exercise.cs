﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Exercise
{
    //Properties
    private int hours;
    private int minutes;
    private string name;
    private string airportCode;

    //Stored flightPlans
    private List<FlightPlanData> flightPlans = new List<FlightPlanData>();



    //Properties Get and Set
    public  List<FlightPlanData> FlightPlans
    {
        get { return flightPlans; }
        set { flightPlans = value; }

    }

    public string Name
    {
        get { return name; }
        set { name = value; }

    }

    public string AirportCode
    {
        get { return airportCode; }
        set { airportCode = value; }

    }

    public int Hours
    {
        get { return hours; }
        set { hours = value; }

    }

    public int Minutes
    {
        get { return minutes; }
        set { minutes = value; }

    }





    //Create a new flight plan and add it to the list
    public  void CreateFlightPlan()
    {
        FlightPlanData flp = new FlightPlanData();

        //Fill in the flight plan data


        //Insert the flight plan into the list
        InsertFlightPlan(flp);

    }

    //Get a Flight Plan by a given callsign
    public  FlightPlanData GetFlightPlan(string callSign)
    {
        if (flightPlans != null)
        {
            foreach (FlightPlanData flightPlan in flightPlans)
            {
                if (flightPlan.CallSign == callSign)
                {
                    return flightPlan;
                }
            }
        }

        return null;
    }


  


    public  void InsertFlightPlan(FlightPlanData flp)
    {
        flightPlans.Add(flp);
    }


}
 