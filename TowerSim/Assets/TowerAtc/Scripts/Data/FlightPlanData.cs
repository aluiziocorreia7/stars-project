﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



[System.Serializable]
public class FlightPlanData
{
    //private Aircraft aircraft; 

    [Header("Flight Plan Properties")]
    private string callSign;
    private string type;
    private string destination;
    private string origin;
    private string flightLevel;
    private string time;
    private int ssrCode;
    private string registration;
    private string route;
    private string stand = null;
    private int timeInSeconds;
    

    public string CallSign
    {
        get{ return this.callSign; }
        set { callSign = value;}
    }

    public string Type
    {
        get { return this.type; }
        set { type = value; }
    }

    public string Destination
    {
        get {return this.destination; }
        set {destination = value; }
    }
     
    public string Origin
    {
        get { return this.origin;}
        set { this.origin = value; }

    }
 
    public string FlightLevel
    {
        get {return this.flightLevel;}
        set {this.flightLevel = value;}
    }
    
    public string Time
    {
        get { return this.time;}
        set { this.time = value;}
    }

    public int SsrCode
    {
        get { return this.ssrCode; }
        set { this.ssrCode = value; }
    }


    public string Registration
    {
        get {return this.registration; }
        set {this.registration = value; }

    }

    public string Route
    {
        get { return this.route; }
        set { this.route = value; }

    }

    public string Stand
    {
        get { return this.stand; }
        set { stand = value; }
    }

    
    public int TimeInSeconds
    {
        get { return this.timeInSeconds;}
        //set { timeInSeconds = value;}
    }


    public void SetTimeInSeconds()
    {
        int minutes=0, hours=0;

        hours = int.Parse(time.Substring(0,2));      
        minutes = int.Parse(time.Substring(2,2));
        Debug.Log("Hours: " + hours + " Minutes: " + minutes);

        timeInSeconds = (minutes * 60) + (hours * 60 * 60);
        Debug.Log("Aircraft time: " + timeInSeconds);
    }

}
