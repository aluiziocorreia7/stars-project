﻿using UnityEngine;
using System.Collections.Generic;
using Mirror;
using UnityEngine.UI;
using AirportPack;

public class Server : MonoBehaviour
{
    [SerializeField] private GameObject connectedText;
    [SerializeField] private Button startButton;
    [SerializeField] private Button stopButton;
    [SerializeField] private Transform clientsConnectedParent;
    [SerializeField] private GameObject connectionPrefab;
    [SerializeField] private Canvas mainMenu = null;
    [SerializeField] private GameObject camera;

    private CameraControl cameraController ;
    private bool isActive = true;


    #region MonoBehaviour's Methods
    private void OnEnable()
    {
        SimNetworkManager.ServerOnConnected += HandleServerConnected;
        SimNetworkManager.ServerOnDisconnected += HandleServerDisconnected;
        SimNetworkManager.ClientOnConnected += HandleClientConnected;
        SimNetworkManager.ClientOnDisconnected += HandleClientDisconnected;

        cameraController = camera.GetComponent<CameraControl>();

    }

    private void OnDisable()
    {
        SimNetworkManager.ServerOnConnected -= HandleServerConnected;
        SimNetworkManager.ServerOnDisconnected -= HandleServerDisconnected;
        SimNetworkManager.ClientOnConnected -= HandleClientConnected;
        SimNetworkManager.ClientOnDisconnected -= HandleClientDisconnected;

    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isActive = !isActive;
            mainMenu.gameObject.SetActive(isActive);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            cameraController.enabled = !cameraController.enabled;
        }
    }
    #endregion


    //Called when startButton is clicked
    public void StartServer()
    {
        
        NetworkManager.singleton.StartServer();
   
    }

    //Called when stopButton is clicked
    public void StopServer()
    {
        NetworkManager.singleton.StopServer();
    }


    

    //Called when the Server is connected
    private void HandleServerConnected()
    {
        connectedText.SetActive(true);
        startButton.interactable = false;
        stopButton.gameObject.SetActive(true);
    }

    //Called when the Server disconnects
    private void HandleServerDisconnected()
    {
        connectedText.SetActive(false);
        startButton.interactable = true;
        stopButton.gameObject.SetActive(false);

        foreach (Transform item in clientsConnectedParent.GetComponentInChildren<Transform>())
        {
                Destroy(item.gameObject);    
        }
    }

    //Called when a client connects to the server
    private void HandleClientConnected(PlayerObject player)
    {
        GameObject clientConnectionItem = Instantiate(connectionPrefab, clientsConnectedParent, true);
        clientConnectionItem.name = player.name;
        clientConnectionItem.GetComponentInChildren<Text>().text = player.name;
    }

    //Called when a client disconnects to the server
    private void HandleClientDisconnected(PlayerObject player)
    {
        foreach (Transform item in clientsConnectedParent.GetComponentInChildren<Transform>())
        {
            if(item.name == player.name)
            {
                Destroy(item.gameObject);
                return;
            }
        }  

    }

}
