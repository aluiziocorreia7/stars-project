﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;



public enum FlightPlanState
{
    ARRIVAL,
    DEPARTURE
}


public class FlightPlan : NetworkBehaviour
{
    [SyncVar]
    private FlightPlanState _flightPlanState;

    [Header("Flight Plan Properties")]

    [SyncVar][SerializeField]
    private string _callSign;
    [SyncVar]
    private string _type;
    [SyncVar]
    private string _destination;
    [SyncVar]
    private string _origin;
    [SyncVar]
    private string _flightLevel;
    [SyncVar]
    private string _time;
    [SyncVar]
    private int _ssrCode;
    [SyncVar]
    private string _registration;
    [SyncVar]
    private string _route;
    [SyncVar]
    private string _stand = null;
    [SyncVar]
    private int _timeInSeconds;

    private Transform _aircraft= null;
  
    //Ainda por decidir como guardar as informaçoes
    private string _Sid = null;
    private string _Star = null;



    public void Setup(FlightPlanData flpData,string airportCode)
    {
        _callSign = flpData.CallSign;
        _type = flpData.Type;
        _destination = flpData.Destination;
        _origin = flpData.Origin;
        _flightLevel = flpData.FlightLevel;
        _time = flpData.Time;
        _ssrCode = flpData.SsrCode;
        _registration = flpData.Registration;
        _route = flpData.Route;
        _stand = flpData.Stand;
        _timeInSeconds = flpData.TimeInSeconds;
        SetFlightPlanState(airportCode);


    }

    public int TimeInSeconds
    {
        get { return this._timeInSeconds; }
        set { _timeInSeconds = value; }
    }

    public string CallSign
    {
        get{ return this._callSign; }
        set { _callSign = value;}
    }

    public string Type
    {
        get { return this._type; }
        set { _type = value; }
    }

    public string Destination
    {
        get {return this._destination; }
        set {_destination = value; }
    }
     
    public string Origin
    {
        get { return this._origin;}
        set { this._origin = value; }

    }
 
    public string FlightLevel
    {
        get {return this._flightLevel;}
        set {this._flightLevel = value;}
    }
    
    public string Time
    {
        get { return this._time;}
        set { this._time = value;}
    }
   
     public int SsrCode
    {
        get { return this._ssrCode; }
        set { this._ssrCode = value; }
    }
  
    public string Registration
    {
        get {return this._registration; }
        set {this._registration = value; }

    }

    public string Route
    {
        get { return this._route; }
        set { this._route = value; }

    }

    public string Stand
    {
        get { return this._stand; }
        set { _stand = value; }
    }


    public FlightPlanState FlightPlanState
    {
        get { return _flightPlanState; }
    }


    public void SetFlightPlanState(string airportCode)
    {
        if (_origin == airportCode)
        {

            _flightPlanState = FlightPlanState.DEPARTURE;
        }
        else
        {
            _flightPlanState = FlightPlanState.ARRIVAL;
        }
    }


    public Transform GetAircraft()
    {
        return _aircraft;

    }

    public void SetAircraft(Transform aircraft )
    {
        this._aircraft = aircraft;
    }
   
}
