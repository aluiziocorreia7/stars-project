﻿using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerObject : NetworkBehaviour
{  
    private Camera camera;
    private GameObject selectedAircraft;
    private string selectedCallsign;
    private GameObject highlighted;
    public UIPilot uiPilot;
    private WaypointsManager waypointsManager;
    
    private const float feetToMeters = 0.3048f;
    private bool isRunning = false;
    


    // Start is called before the first frame update
    void Start()
    {
        if (!isServer)
        {   
            camera = Camera.main;
            uiPilot = FindObjectOfType<UIPilot>();
            CursorManager.Instance.OnCursorChanged += Instance_OnCursorChanged;
            waypointsManager = GameObject.Find("Taxiways").GetComponent<WaypointsManager>();
            
            //UIPilot Buttons
            uiPilot.taxiButton.onClick.AddListener(ExecuteTaxi);
            uiPilot.takeOffButton.onClick.AddListener(ExecuteTakeOff);
            uiPilot.holdPositionButton.onClick.AddListener(ExecuteHoldPosition);
            uiPilot.continueTaxiButton.onClick.AddListener(ExecuteContinueTaxi);
            uiPilot.pushAndStartButton.onClick.AddListener(ExecutePushAndStart);
            Debug.Log(" START FUNCTION PlayerObject");
            
        }

        DontDestroyOnLoad(this);
    }
    
   

    // Update is called once per frame
    void Update()
    {
        //if (!isRunning) return;

        if (isLocalPlayer == false || isServer == true)
            return;


        //Get the left mouse clicked position
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {

                if (selectedAircraft == null)
                {

                    //Checks for aircraft selection
                    if (hit.collider.transform.root.tag == "aircraft")
                    {    
                        selectedCallsign = hit.collider.transform.root.GetComponent<FlightPlan>().CallSign;
                        Debug.Log(selectedCallsign);
                        selectedAircraft = hit.collider.transform.root.gameObject;
                        Debug.Log("Selected Aircraft: " +selectedAircraft.GetComponent<FlightPlan>().CallSign);
                        SetSelectedVisible(selectedAircraft, true);
                        waypointsManager.SetVisibleTaxiways(selectedCallsign ,true);
                        
                        if (uiPilot != null)
                            uiPilot.Callsign(selectedCallsign);

                    }

                    return;
                }


                if (hit.collider.transform.tag == "taxiway")
                {
                    Debug.Log("Hit a collider taxiway");
                    Transform taxiway = hit.collider.transform;
                    LineRenderer lr = taxiway.GetComponent<LineRenderer>();
                    lr.enabled = true;
                    Vector3[] waypoints = new Vector3[lr.positionCount];
                    lr.GetPositions(waypoints);
 
                    waypointsManager.CreatePath(selectedCallsign,taxiway.name,waypoints);

                    return;
                }


                if (hit.collider.transform.root.tag == "aircraft" && hit.collider.transform.root.GetComponent<FlightPlan>().CallSign != selectedCallsign)
                {
                    SetSelectedVisible(selectedAircraft, false);
                    selectedCallsign = hit.collider.transform.root.GetComponent<FlightPlan>().CallSign;
                    selectedAircraft = hit.collider.transform.root.gameObject;
                    SetSelectedVisible(selectedAircraft, true);

                    if (uiPilot != null)
                        uiPilot.Callsign(selectedCallsign);

                    return;
                }

                
                /*
                //Add waypoint to the path 
                if (hit.collider.transform.tag == "ground")
                {
                    Debug.Log("HIT GROUND");
                    float altitudeMeters = uiPilot.currentAltitude * feetToMeters;  //Convert fts to meters
                    Vector3 waypointPosition = new Vector3(hit.point.x, altitudeMeters, hit.point.z);

                    //Save the waypoint to the WaypointPath
                    SaveWaypoint(waypointPosition, selectedCallsign);
                }
                */
            }

        }


        //Remove Selected Right mouse button
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                //Click over a selected aircraft, remove selection
                if (selectedAircraft != null && hit.collider.transform.root.name == selectedAircraft.name)
                {
                    SetSelectedVisible(selectedAircraft, false);
                    CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);
                    uiPilot.Callsign(string.Empty);
                    waypointsManager.SetVisibleTaxiways(selectedCallsign,false);
                    selectedAircraft = null;
                    return;
                }

                //Click over a waypoint, to delete it
                if(hit.collider.transform.tag == "waypoint")
                {
                   
                    GameObject go = hit.collider.transform.gameObject;
                    Destroy(go);
                    return;
                }


                if (hit.collider.transform.tag == "taxiway")
                {
                    
                    hit.collider.transform.GetComponent<LineRenderer>().enabled = false;
                    waypointsManager.RemoveTaxiwayFromPath(selectedCallsign, hit.collider.transform.name);
                    return;
                }

            }

        }
    }


    private void Instance_OnCursorChanged(object sender, CursorManager.OnCursorChangedEventArgs e)
    {
        if (e.cursorType == CursorManager.CursorType.Arrow)
        {
            if (selectedAircraft != null)
            {
                CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Move);
            }
        }
    }


    private void SetSelectedVisible(GameObject gameObject, bool visible)
    {
        GameObject highlighted = gameObject.transform.Find("Selected").gameObject;
        highlighted.SetActive(visible);
    }

  
    #region UI PILOT METHODS
    public void ExecuteTaxi()
    {
        Vector3[] path = waypointsManager.GetTaxiwaysNodes(selectedCallsign, selectedAircraft.transform);
        
        if (path != null)
        {
            
            CmdTaxi(selectedAircraft, path);
            Debug.Log("Call CmdTaxi:");
            waypointsManager.SetVisibleTaxiways(selectedCallsign, false);
            waypointsManager.RemovePathByCallsign(selectedCallsign);
            
        }

    }


    public void ExecutePushAndStart()
    {
        Vector3[] path = waypointsManager.GetTaxiwaysNodes(selectedCallsign, selectedAircraft.transform);
        if (path != null)
        {
            Debug.Log("Call PushAndStart:");
            CmdPushAndStart(selectedAircraft, path);
            waypointsManager.SetVisibleTaxiways(selectedCallsign, false);
            waypointsManager.RemovePathByCallsign(selectedCallsign);

        }

    }


    public void ExecuteTakeOff()
    {

        CmdTakeOff(selectedAircraft);
        Debug.Log("Call CmdTakeOff");

    }

    public void ExecuteHoldPosition()
    {
        CmdHoldPosition(selectedAircraft);
        Debug.Log("Call CmdHoldPosition");
    }

    public void ExecuteContinueTaxi()
    {
        CmdContinueTaxi(selectedAircraft);
        Debug.Log("Call CmdContinueTaxi");
    }
#endregion

    #region Client Callbacks
    public override void OnStartClient()
    {
        this.name = "Pilot:" + (((SimNetworkManager)NetworkManager.singleton).Players.Count + 1);
        ((SimNetworkManager)NetworkManager.singleton).Players.Add(this);
    }


    public override void OnStopClient()
    {
        ((SimNetworkManager)NetworkManager.singleton).Players.Remove(this);
    }

    #endregion

    #region Server [Command]

    //Move the aircraft in the Server

    [Command]
    void CmdTaxi(GameObject aircraft, Vector3[] path)
    {
       Debug.Log("Call the Server to move the aircraft: "+ aircraft.name);
       AirplaneAiControl aiController = aircraft.GetComponent<AirplaneAiControl>();

        //Hide taxi_btn , Show HoldPosition_btn, Show CancelTaxi_btn
        aiController.SetPath(path);
        aiController.GetComponent<Animator>().SetTrigger("taxi");

    }

    [Command]
    void CmdTakeOff(GameObject aircraft)
    {
        Debug.Log("Call the Server to take off the aircraft: " + aircraft.name);
        aircraft.GetComponent<Animator>().SetTrigger("takeOff");

    }

    [Command]
    void CmdHoldPosition(GameObject aircraft)
    {
        Debug.Log("Call the Server to HoldPosition ... Aircraft: " + aircraft.name);
        aircraft.GetComponent<AirplaneAiControl>().ApplyBrakes(1,true);

    }

    [Command]
    void CmdContinueTaxi(GameObject aircraft)
    {
        Debug.Log("Call the Server to ContinueTaxi ... Aircraft: " + aircraft.name);
        aircraft.GetComponent<AirplaneAiControl>().ApplyBrakes(0,false);
    }

    [Command]
    void CmdPushAndStart(GameObject aircraft, Vector3[] path)
    {
        Debug.Log("Call the server to Push and Start ... Aircraft: " + aircraft.name);
        aircraft.GetComponent<AirplaneAiControl>().SetPath(path);
        aircraft.GetComponent<Animator>().SetTrigger("pushAndstart");
        
    }

    [Command]
    void CmdSpawnAircraft()
    {
        /*
        Debug.Log("Aircraft Spawner");

        GameObject cessna = Instantiate(cessnaPrefab, cessnaPrefab.transform.position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(cessna);
        */
    }
    #endregion


}
