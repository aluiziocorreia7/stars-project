﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using System;

public class SimNetworkManager : NetworkManager
{
    public List<PlayerObject> Players { get; } = new List<PlayerObject>();

    public static event Action ServerOnConnected;
    public static event Action ServerOnDisconnected;
    public static event Action<PlayerObject> ClientOnConnected;
    public static event Action<PlayerObject> ClientOnDisconnected;


    #region Server Functions

    [Server]
    public void StartSimTower()
    {
        if (Players.Count == 0) return;
        ServerChangeScene("GVAC");
    }
    
    public override void OnStartServer()
    {
        base.OnStartServer();
        ServerOnConnected?.Invoke();
    }

    public override void OnStopServer()
    {
        base.OnStopServer();
        ServerOnDisconnected?.Invoke();
        Players.Clear();
    }

    
    
    //Called when a client is added to the server
    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        base.OnServerAddPlayer(conn);
        PlayerObject player = conn.identity.GetComponent<PlayerObject>();
        Players.Add(player);
        player.name = "Pilot:" + Players.Count;
        ClientOnConnected?.Invoke(player);    
    }

    //Called when a client disconnects from the server
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        if (Players.Count != 0)
        {
            PlayerObject player = conn.identity.GetComponent<PlayerObject>();
            Players.Remove(player);
            ClientOnDisconnected?.Invoke(player);
        }
        base.OnServerDisconnect(conn);
      
    }

    #endregion


    #region Client Functions

    public override void OnStopClient()
    {
        base.OnStopClient();
        Players.Clear();
    }


    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        ClientOnConnected?.Invoke(null);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        ClientOnDisconnected?.Invoke(null);
    }

    #endregion



}
