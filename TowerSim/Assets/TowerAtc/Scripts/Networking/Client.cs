﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Mirror;
using UnityEngine.UI;


public class Client : MonoBehaviour
{
    
    [SerializeField] private TMP_InputField addressInput = null;
    [SerializeField] private Button connectButton = null;
    [SerializeField] private Button disconnectButton = null;
    [SerializeField] private Transform clientConnectionItem;
    [SerializeField] private Canvas mainMenu = null;

    bool isActive = true;


    #region MonoBehaviour's Methods
    private void OnEnable()
    {
        SimNetworkManager.ClientOnConnected += HandleClientConnected;
        SimNetworkManager.ClientOnDisconnected += HandleClientDisconnected;
    }

    private void OnDisable()
    {
        SimNetworkManager.ClientOnConnected -= HandleClientConnected;
        SimNetworkManager.ClientOnDisconnected -= HandleClientDisconnected;
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isActive = !isActive;
            mainMenu.gameObject.SetActive(isActive);
        }
    }

    #endregion


    public void ConnectToServer()
    {
        
        string address = addressInput.text;
        NetworkManager.singleton.networkAddress = address;
        NetworkManager.singleton.StartClient();
    }

    public void DisconnectFromServer()
    {
        NetworkManager.singleton.StopClient();

    }


    private void HandleClientConnected(PlayerObject player)
    {
        connectButton.interactable = false;
        addressInput.interactable = false;
        disconnectButton.interactable = true;
        clientConnectionItem.gameObject.SetActive(true);
    }

    private void HandleClientDisconnected(PlayerObject player)
    {
        connectButton.interactable = true;
        addressInput.interactable = true;
        disconnectButton.interactable = false;

        clientConnectionItem.gameObject.SetActive(false);
    }


   

}
