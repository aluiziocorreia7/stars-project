﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using IndiePixel;

public class Spawner : NetworkBehaviour
{
   

    public override void OnStartServer()
    {
        SimManager._instance._networkSpawner = this;
        //SimManager._instance.Setup();
        
    }


    public Transform SpawnAircraftOnStand(GameObject prefab, Transform stand, FlightPlanData flpData)
    {
        
        Vector3 spawnPosition = new Vector3(stand.position.x, stand.position.y + 1, stand.position.z);
        GameObject aircraft = Instantiate(prefab, spawnPosition, stand.rotation);
        NetworkServer.Spawn(aircraft);
        Debug.Log("Aircraft Spawned on Stand");
        return aircraft.transform;
    }

    public void SpawnAircraftOnFlight(GameObject aircraft, Transform arrival)
    {
        //GameObject aircraft = Instantiate(prefab, arrival.position, arrival.rotation);
        NetworkServer.Spawn(aircraft);
        Debug.Log("Aircraft Spawned on Flight");

        //return aircraft.transform;
    }



    /*
    public void SpawnAircraft()
    {
        if (isServer)
        {
            
            GameObject Tap1542 = Instantiate(cessnaPrefab, cessnaPrefab.transform.position, Quaternion.identity);
            Tap1542.name = "TAP1542";
            //Tap1542.GetComponent<FlightPlan>().CallSign = Tap1542.name;
            //Debug.Log("SERVER::" + Tap1542.GetComponent<FlightPlan>().CallSign);
            NetworkServer.Spawn(Tap1542);
        }

           /*
        GameObject Tcv661 = Instantiate(cessnaPrefab, offset, Quaternion.identity);
        Tcv661.name = "TCV661";
        //Tcv661.GetComponent<FlightPlan>().CallSign = Tcv661.name;
        //Debug.Log("SERVER::" + Tap1542.GetComponent<FlightPlan>().CallSign);
        NetworkServer.Spawn(Tcv661);
       } */
    

 }
