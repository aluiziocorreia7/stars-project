﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaxiFSM : BaseFSM
{
    Vector3[] _path;
    private float switchDistance = 5f;
    int index;
    bool isTaxing = false;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        _path = aiController.GetPath();
        aiController.ApplyBrakes(0,false);
        index = 0;
        isTaxing = true;
        Debug.Log("ENTER TAXIFSM ");
       
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!isTaxing) return;
        
        if (_path == null || _path.Length == 0) return;
        Debug.Log("UPDATE TAXIFSM ");

        float dist = Vector3.Distance(aircraft.position, _path[index]);
        if ( dist >= switchDistance)
        {
            //Debug.Log("Distance : " + dist);
            aiController.Taxi(_path[index]);

        }
        else
        {
            index++;
            if (index >= _path.Length)
            {
                _path = null;
                aiController.SetPath(null);
                aiController.ApplyBrakes(1f,true);
                animator.SetTrigger("stop");
                Debug.Log("STOP TAXI");

            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isTaxing = false;
       Debug.Log("EXIT STATE TAXI");
    }

    
}
