﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushAndStartFSM : BaseFSM
{
    private bool isPushingBack = false;
    Vector3[] _path;
    private float switchDistance = 1f;
    int index;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        _path = aiController.GetPath();
        aiController.ApplyBrakes(0,false);
        index = 0;
        isPushingBack = true;
        Debug.Log("ENTER PUSHBACK FSM ");

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!isPushingBack) return;

        float dist = Vector3.Distance(aircraft.position, _path[index]);
        if (dist >= switchDistance)
        {
            //Debug.Log("Distance : " + dist);
            aiController.PushAndStart(_path[index]);

        }
        else
        {
            index++;
            if (index >= _path.Length)
            {
                aiController.SetPath(null);
                aiController.ApplyBrakes(1f,true);
                animator.SetTrigger("stop");
            }
        }

        Debug.Log("PUSH AND START FSM");

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isPushingBack = false;
    }

   
}
