﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseFSM : StateMachineBehaviour
{
    protected Transform aircraft;
    protected AirplaneAiControl aiController;
    
   


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        aircraft = animator.gameObject.transform;
        aiController = aircraft.GetComponent<AirplaneAiControl>();
    }

}
