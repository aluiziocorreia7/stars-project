﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyFSM : BaseFSM
{
    
    [SerializeField] private Transform target;
    private Vector3 targetPosition;
    private bool isFlying;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        isFlying = true;

        if (target!= null)
            targetPosition = target.position;

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!isFlying) return;

        if (aiController.autoPilot || target == null)
        {
            Vector3 localTarget = aiController.transform.position + new Vector3(0, 0.5f, 5);
            aiController.Fly(localTarget);
            Debug.Log("FLYING STATE AUTO PILOT");
            return;
        }
        
           aiController.Fly(targetPosition);
           if (Vector3.Distance(aircraft.position, targetPosition) <= 5)
               targetPosition = aircraft.position + new Vector3(0, 0.5f, 5);

       
    }


    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isFlying = false;
    }

   
}
