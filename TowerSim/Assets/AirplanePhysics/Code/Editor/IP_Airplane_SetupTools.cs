﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace IndiePixel
{

    public static class IP_Airplane_SetupTools
    {
        public static void BuildDefaultAirplane(string airplaneName)
        {

            //Create the rootGO
            GameObject rootGO = new GameObject(airplaneName, typeof(IP_Airplane_Controller), typeof(IP_BaseAirplane_Input));
            //Create the center of gravity
            GameObject cogGO = new GameObject("CenterOfGravity");
            cogGO.transform.SetParent(rootGO.transform,false);


            //Create the Base Components or Find them
            IP_BaseAirplane_Input input = rootGO.GetComponent<IP_BaseAirplane_Input>();
            IP_Airplane_Controller controller = rootGO.GetComponent<IP_Airplane_Controller>();
            IP_Airplane_Characteristics characteristics = rootGO.GetComponent<IP_Airplane_Characteristics>();

            if(controller)
            {
                //Assign core components
                controller.input = input;
                controller.characteristics = characteristics;
                controller.centerOfGravity = cogGO.transform;

                //Create Structure
                GameObject graphicsGrp = new GameObject("Graphics_GRP");
                GameObject collidersGrp = new GameObject("Colliders_GRP");
                GameObject controlSurfaceGrp = new GameObject("ControlSurface_GRP");

                graphicsGrp.transform.SetParent(rootGO.transform, false);
                collidersGrp.transform.SetParent(rootGO.transform, false);
                controlSurfaceGrp.transform.SetParent(rootGO.transform, false);

                //Create first Engine
                GameObject engineGO = new GameObject("Engine",typeof (IP_Airplane_Engine));
                IP_Airplane_Engine engine = engineGO.GetComponent<IP_Airplane_Engine>();
                controller.engines.Add(engine);
                engineGO.transform.SetParent(rootGO.transform,false);

                //Create the Base Airplane
                GameObject defaultAirplane = (GameObject) AssetDatabase.LoadAssetAtPath("Assets/AirplanePhysics/Art/Objects/Airplanes/Indie-Pixel_Airplane/IndiePixel_Airplane.fbx", typeof(GameObject));
                if (defaultAirplane)
                {
                    GameObject.Instantiate(defaultAirplane,graphicsGrp.transform);

                }  
            }

            //Select the airplane setup
            Selection.activeGameObject = rootGO;

        }
}

}