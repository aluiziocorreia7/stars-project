﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IP_Airplane_GroundEffect : MonoBehaviour
{
    public float maxGroundDistance = 3f;
    public float liftForce = 100f;
    public float maxSpeed = 15f; //meters per second

    private Rigidbody rb;
    
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rb)
        {
            HandleGroundEffect();

        }
    }


    protected virtual void HandleGroundEffect()
    {
        RaycastHit hit;

        //Checks if the ray hits the ground
        if (Physics.Raycast(transform.position,Vector3.down, out hit))
        {
            if(hit.transform.tag == "ground" && hit.distance < maxGroundDistance)
            {
                //Debug.Log("Hitting the ground");
                float currentSpeed = rb.velocity.magnitude;
                float normalizedSpeed = currentSpeed / maxSpeed;
                normalizedSpeed = Mathf.Clamp01(normalizedSpeed);

                float distance = maxGroundDistance - hit.distance;
                //Adds force only when the aiplane is moving
                float finalForce = liftForce * distance * normalizedSpeed;
                rb.AddForce(Vector3.up * finalForce);
            }
        }

    }

}
