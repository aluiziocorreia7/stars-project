﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IP_Airplane_EngineCutOff : MonoBehaviour
{
    [Header("Engine Cut Off Properties")]
    public KeyCode cutOffKey = KeyCode.O;
    public UnityEvent onEngineCutOff = new UnityEvent();

    // Start is called before the first frame update
    void Start()
    {
        
    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(cutOffKey))
        {
            HandleEngineCutOff();
        }
    }


    void HandleEngineCutOff()
    {
        if(onEngineCutOff != null)
        {
            onEngineCutOff.Invoke();
        }
    }

}
