﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





namespace IndiePixel
{
public class IP_BaseAirplane_Input : MonoBehaviour {

	#region Variables
	protected float _pitch = 0f;
	protected float _roll = 0f;
	protected float _yaw = 0f;
	protected float _throttle = 0f;
    

	
	
	protected float stickyThrottle;
    protected bool autopilot = true;

	#endregion
	
	[Header("Throttle Properties")]
	public float throttleSpeed=0.1f;
	
	public float StickyThrottle{
			get{return stickyThrottle;}
		}

    [SerializeField]
    private KeyCode brakeKey = KeyCode.Space;
    protected float brake = 0f;
    
    [SerializeField]
    protected KeyCode cameraKey= KeyCode.C;
    protected bool cameraSwitch = true;

    public int maxFlapIncrements = 2;
	protected int flaps = 0;
    
	#region Properties
	public float Pitch{
		get{return _pitch;}
        set { _pitch = value;}
	}

	public float Roll{
		get{return _roll;}
        set { _roll = value; }
     }

	public float Yaw{
		get{return _yaw;}
        set { _yaw = value; }
    }

	public float Throttle{
		get {return _throttle;}
        set { _throttle = value; }
    }

	public int Flaps{
		get{return flaps;}
        set{ flaps = value;}
    }

    
    public float NormalizedFlaps
        {
            get
            {
                return (float) flaps / maxFlapIncrements;
            }
        }

	public float Brake{
		get{return brake;}
        set {brake = value;}
    }

    public bool CameraSwitch
        {
            get { return cameraSwitch;}
        }

	#endregion

	
	#region Builtin Methods
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
            //Auto pilot disabled, get input from the player
            if (!autopilot)
            {
                HandleInput();
                Debug.Log("Player Pilot");
            }


            StickyThrottleControl();
        }

	#endregion


	protected virtual void HandleInput(){
		//Process Main Controls Inputs
		_pitch = Input.GetAxis("Vertical");
		_roll = Input.GetAxis("Horizontal");
		_yaw = Input.GetAxis("Yaw");
		_throttle = Input.GetAxis("Throttle");
		StickyThrottleControl ();
		
		//Process Brake Inputs
		brake = Input.GetKey(brakeKey)? 1f: 0f ;
	
		//Process Flaps Inputs [0-3]
		if(Input.GetKeyDown(KeyCode.F)){
			flaps += 1;
		}

		if(Input.GetKeyDown(KeyCode.G)){
			flaps -= 1;
		}

		flaps = Mathf.Clamp(flaps,0,maxFlapIncrements);

        //Camera Switch Key
        cameraSwitch = Input.GetKeyDown(cameraKey);
	}

	
	//Controls the throttle sensibility 
	protected virtual void StickyThrottleControl(){
			
			stickyThrottle = stickyThrottle + (_throttle * throttleSpeed * Time.deltaTime);
			stickyThrottle = Mathf.Clamp01 (stickyThrottle);

        }


    //This function is called by the AiControls, updating the input.
    public void AutoPilotInputs(float pitch, float roll, float yaw, float throttle)
        {
            _pitch = Mathf.Clamp(pitch, -1, 1);
            _roll = Mathf.Clamp(roll, -1, 1);
            _yaw = Mathf.Clamp(yaw, -1, 1);
            _throttle = Mathf.Clamp(throttle, -1, 1);
           

        }



    }
}