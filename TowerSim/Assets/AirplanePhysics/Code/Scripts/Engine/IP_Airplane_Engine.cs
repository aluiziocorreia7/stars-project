﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IndiePixel{

[RequireComponent(typeof(IP_Airplane_Fuel))]
public class IP_Airplane_Engine : MonoBehaviour {

	//Variables
	[Header("Engine Properties")]
	public float maxForce = 200f;		//Max Power Force
	public float maxRPM = 2550f;        //Max revolution per minutes
    public float shutOffSpeed = 2f;

	public AnimationCurve powerCurve = AnimationCurve.Linear(0f,0f,1f,1f);

	[Header("Propellers")]
	public IP_Airplane_Propeller propeller;

    private bool isShutOff = false;
    private float lastThrottleValue;
    private float finalShutOffThrottleValue;
    private float currentRPM;

    private IP_Airplane_Fuel fuel;
    
    
  

        
    //Properties

        public bool ShutEngineOff
        {
            set { isShutOff = value; }

        }

        public float CurrentRPM
        {
            get { return currentRPM; }
        }

        //Built in methods
        private void Start()
        {
            if (!fuel)
            {
                fuel = GetComponent<IP_Airplane_Fuel>();
                if (fuel)
                {
                    fuel.InitFuel();
                }
            }

            
        }



        //Custom Methods
        public Vector3 CalculateForce(float throttle)
        {

            //Clamp the values between 0 and 1
            float finalThrottle = Mathf.Clamp01(throttle);

            if (!isShutOff)
            {
                //Uses the power curve to set the amount of throttle
                finalThrottle = powerCurve.Evaluate(finalThrottle);
                lastThrottleValue = finalThrottle;
            }
            else
            {
                lastThrottleValue -= Time.deltaTime * shutOffSpeed;
                lastThrottleValue = Mathf.Clamp01(lastThrottleValue);
                finalShutOffThrottleValue = powerCurve.Evaluate(lastThrottleValue);
                finalThrottle = finalShutOffThrottleValue;
            }



            //Calculate RPM's
            currentRPM = finalThrottle * maxRPM;

                if (propeller)
                {
                    propeller.HandlePropeller(currentRPM);
                }


            //HandleFuel(finalThrottle);


               

                //Create Force
                float finalPower = finalThrottle * maxForce;
                Vector3 finalForce = transform.forward * finalPower;

                return finalForce;
                       
        }
        
        void HandleFuel(float throttleValue)
        {
            if (fuel)
            {
                fuel.UpdateFuel(throttleValue);

                if(fuel.CurrentFuel <= 0f)
                {
                    isShutOff = false;
                }
            }
        }
}
}