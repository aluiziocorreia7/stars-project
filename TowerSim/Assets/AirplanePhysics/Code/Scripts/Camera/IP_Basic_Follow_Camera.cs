﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IP_Basic_Follow_Camera : MonoBehaviour
{

    [Header("Basic Follow Camera Properties")]
    public Transform target;
    public float distance = 4f;
    public float height = 2f;
    public float smoothSpeed = 0.5f;

    private Vector3 smoothVelocity;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    
    void FixedUpdate()
    {
        if (target)
        {
            HandleCamera();
        }
    }

    protected virtual void HandleCamera()
    {
        //Position the camera 
        Vector3 wantedPosition = target.position + (-target.forward * distance) + (Vector3.up*height);
        //Debug.DrawLine(target.position, wantedPosition, Color.blue);

        transform.position = Vector3.SmoothDamp(transform.position,wantedPosition,ref smoothVelocity,smoothSpeed);
        //Camera looking at the target
        transform.LookAt(target);
    }
}
