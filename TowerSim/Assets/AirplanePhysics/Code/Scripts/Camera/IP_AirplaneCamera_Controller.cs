﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IndiePixel
{
    public class IP_AirplaneCamera_Controller : MonoBehaviour
    {

        [Header("Camera Controller Properties")]
        public IP_BaseAirplane_Input input;
        public int startCameraIndex = 0;
        public List<Camera> cameras = new List<Camera>();

        private int cameraIndex = 0;

        // Start is called before the first frame update
        void Start()
        {
            if(startCameraIndex >= 0 && startCameraIndex < cameras.Count)
            {
                DisableAllCameras();
                cameras[startCameraIndex].enabled = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (input)
            {
                if (input.CameraSwitch)
                {
                    SwitchCamera();
                }
            }
        }


        protected virtual void SwitchCamera()
        {
            if (cameras.Count > 0)
            {
                DisableAllCameras();

                //Increment camera index
                cameraIndex++;
                //Wrap index
                if (cameraIndex >= cameras.Count)
                {
                    cameraIndex = 0;
                }

                //Enabled Next camera
                cameras[cameraIndex].enabled = true;
                cameras[cameraIndex].GetComponent<AudioListener>().enabled = true;
            }
        }

        void DisableAllCameras()
        {
            foreach(Camera cam in cameras)
            {
                cam.enabled = false;
                cam.GetComponent<AudioListener>().enabled = false;
            }
        }
    }
}