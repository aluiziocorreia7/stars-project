﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IndiePixel{

[RequireComponent(typeof(WheelCollider))]
public class IP_Airplane_Wheel : MonoBehaviour {
    [Header("Wheel Properties")]
    public Transform wheelGraphics; 
    public bool isBraking = false;
    public float brakePower = 5f;
    public bool isSteering = false;
    public float steerAngle = 20f;
    public float steerSmoothSpeed=2f;
    

	private WheelCollider wheelCol;
    private Vector3 worldPos;
    private Quaternion worldRot;
    private float finalBrakeForce;
    private float finalSteerAngle;


	//Builtin Methods
	public void Start(){

		wheelCol= GetComponent<WheelCollider>();

	}


	//Custom Methods
	public void InitWheel(){

		if(wheelCol){
                //Unlock the wheelcollider to be able to roll
                wheelCol.motorTorque = 0.000000000000001f;
                
            }
	}


      public void HandleWheel(IP_BaseAirplane_Input input)
        {
            if (wheelCol)
            {
                wheelCol.GetWorldPose(out worldPos, out worldRot);
                if (wheelGraphics)
                {
                    wheelGraphics.position = worldPos;
                    wheelGraphics.rotation = worldRot;
                }

                if (isBraking)
                {

                    if (input.Brake > 0.1f)
                    {
                        finalBrakeForce = Mathf.Lerp(finalBrakeForce, input.Brake * brakePower, Time.deltaTime);
                        wheelCol.brakeTorque = finalBrakeForce;
                    }
                    else
                    {
                        finalBrakeForce = 0f;
                        wheelCol.brakeTorque = 0f;
                        wheelCol.motorTorque = 0.0000000000001f;
                    }
                }
            }

                if (isSteering)
                 {
                  finalSteerAngle = Mathf.Lerp(finalSteerAngle, -input.Yaw * steerAngle, Time.deltaTime*steerSmoothSpeed);    
                  wheelCol.steerAngle = finalSteerAngle;

                }
        }
}

}