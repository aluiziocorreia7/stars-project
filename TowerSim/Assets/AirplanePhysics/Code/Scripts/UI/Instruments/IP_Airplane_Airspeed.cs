﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IndiePixel
{

    public class IP_Airplane_Airspeed : MonoBehaviour, IAirplaneUI
    {
        // Variables
        [Header("AirSpeed Indicator Properties")]
        public IP_Airplane_Characteristics characteristics;
        public RectTransform pointer;
        public float maxIndicatedKnots = 160f;

        public const float mphToKnots = 0.868976f;


        // Interface Methods
        public void HandleAirplaneUI()
        {
            if(characteristics && pointer)
            {
                float currentKnots = characteristics.mph * mphToKnots;
                float normalizedKnots = Mathf.InverseLerp(0f, maxIndicatedKnots, currentKnots);
                float wantedRotation = 360 * normalizedKnots;
                pointer.rotation = Quaternion.Euler(0f, 0f, -wantedRotation);
            }
        }
    }
}