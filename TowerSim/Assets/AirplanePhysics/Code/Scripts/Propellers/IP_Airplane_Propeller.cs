﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace IndiePixel{
public class IP_Airplane_Propeller : MonoBehaviour {

	[Header("Propeller Properties")]
    public float minRotationRPM =30f;
	public float minQuadRPMs = 300f;
    public float minTextureSwap = 600f;
	public GameObject mainProp;
	public GameObject blurredProp;

    [Header("Material Properties")]
    public Material blurredPropMat;
    public Texture2D blurLevel1;
    public Texture2D blurLevel2;



        void Start(){

		    if(mainProp && blurredProp)
            {
			//Initialize the main propeller true
			HandleSwapping(0f);
		    }
	    }

	//Custom Methods
	public void HandlePropeller(float currentRPM){
		//Get degrees per second
		float degrees = (((currentRPM * 360f) / 60f )+minRotationRPM)* Time.deltaTime;
        //Rotate on the z-axis , a certain degrees
        degrees = Mathf.Clamp(degrees, 0f, minRotationRPM);

        //Rotate the Propeller Group
        transform.Rotate(Vector3.forward, degrees);

		//Handle the propeller swapping
		if(mainProp && blurredProp){
			HandleSwapping(currentRPM);
		}


	}

	//Swap the propeller textures from main to blurred or vice versa 
	void HandleSwapping(float currentRPM){

		if(currentRPM > minQuadRPMs){
			blurredProp.gameObject.SetActive(true);
			mainProp.gameObject.SetActive(false);

                if (blurredPropMat && blurLevel1 & blurLevel2)
                {
                    if (currentRPM > minTextureSwap)
                    {
                        blurredPropMat.SetTexture("MainTex", blurLevel2);
                    }
                    else
                    {
                        blurredPropMat.SetTexture("MainTex", blurLevel1);
                    }
                }  

        }else{
			blurredProp.gameObject.SetActive(false);
			mainProp.gameObject.SetActive(true);
		}

	}
}
}