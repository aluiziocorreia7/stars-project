﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace IndiePixel{
public class IP_Airplane_Characteristics : MonoBehaviour {

        [Header("Characteristics Properties")]
        
		public float forwardSpeed;
		public float mph;
		public float maxMPH = 110f;		//Max Miles Per Hours
        public float rbLerpSpeed = 0.01f;
        public float maxTaxiSpeed = 5f;

		[Header("Lift Properties")]
		public float maxLiftPower = 0f;
		public  AnimationCurve liftCurve= AnimationCurve.EaseInOut (0f,0f,1f,1f);

        [Header("Pitch Properties")]
        public float maxPitchAngle= 30;

        [Header("Drag Properties")]
        public float dragFactor = 0.01f;
        public float flapDragFactor = 0.005f;

        [Header("Control Properties")]
        public float pitchSpeed = 1000f;
        public float rollSpeed = 1000f;
        public float yawSpeed = 1000;



		private Rigidbody _rb;
        private IP_BaseAirplane_Input _input;
		private float startDrag;
		private float startAngularDrag; 
        private float angleOfAttack;
        public float pitchAngle;
        public float rollAngle;


        private float maxMPS; //Max meters for second
		private float normalizedMPH;


		#region Constants
		const float mpsToMph = 2.23691f;
		#endregion



		//Custom Methods
		public void InitCharacteristics(Rigidbody rb, IP_BaseAirplane_Input curInput){
			//Basic Initialization
			_rb = rb;
            _input = curInput;
			startDrag = _rb.drag;
			startAngularDrag = _rb.angularDrag;

			//Find the max Meters Per Second
			maxMPS = maxMPH / mpsToMph;

		}

		public void UpdateCharacteristics(){
			if (_rb) {
				//Process the Flight Model
				CalculateForwardSpeed ();
				CalculateLift ();
				CalculateDrag ();
                HandlePitch();
                HandleRoll();
                HandleYaw();
                HandleBanking();
                HandleRigidbodyTransform();
            }
		}


		void CalculateForwardSpeed(){
			Vector3 localVelocity = transform.InverseTransformDirection (_rb.velocity);
			forwardSpeed = Mathf.Max(0f, localVelocity.z);

            //[0 - maxMetersPerSecond]
            forwardSpeed = Mathf.Clamp (forwardSpeed,0f, maxMPS);

			//Convert to Miles Per Hour
			mph = forwardSpeed * mpsToMph;

			//[0- Miles per Hour]
			mph = Mathf.Clamp (mph,0f,maxMPH);

			//[0-1] to use the value with the animation curve
			normalizedMPH = Mathf.InverseLerp (0f,maxMPH,mph);


		}
			

		void CalculateLift(){
            //Get the angle of attack
            angleOfAttack = Vector3.Dot(_rb.velocity.normalized, transform.forward);
            angleOfAttack *= angleOfAttack;
            //Debug.Log("Angle of Attack: " +angleOfAttack);

            //Create the lift direction
			Vector3 liftDir = transform.up;
            float liftPower = liftCurve.Evaluate(normalizedMPH) * maxLiftPower ;

            //Apply the final lift force for take off
            Vector3 finalLiftForce = liftDir * liftPower;
            _rb.AddForce(finalLiftForce);

            //Debug.Log("Final Force :" + finalLiftForce);
           

		}

		void CalculateDrag(){
            //Speed Drag
            float speedDrag = forwardSpeed * dragFactor;

            //Flap Drag
            float flapDrag = _input.Flaps * flapDragFactor;

            //Add it all together
            float finalDrag = startDrag + speedDrag + flapDrag;

            _rb.drag = finalDrag;
            _rb.angularDrag = startAngularDrag * forwardSpeed;
		}


        void HandlePitch() { 
            Vector3 flatforward = transform.forward;
            flatforward.y = 0f;
            flatforward = flatforward.normalized;
            pitchAngle = Vector3.Angle(transform.forward, flatforward);
            //Debug.Log("PitchAngle: " +pitchAngle);
            Vector3 pitchTorque = _input.Pitch * pitchSpeed * transform.right;

            //Rotate the airplane on the X-Axis
            if (pitchAngle < maxPitchAngle)
             _rb.AddTorque(pitchTorque);

        }


        void HandleRoll() {
            Vector3 flatRight = transform.right;
            flatRight.y = 0;
            flatRight = flatRight.normalized;
            rollAngle = Vector3.SignedAngle(transform.right, flatRight,transform.forward);

            //Rotate the airplane on the Z-Axis
            Vector3 rollTorque = -_input.Roll * rollSpeed * transform.forward;
            _rb.AddTorque(rollTorque);


        }

        void HandleBanking()
        {
            //Bankside Between[0,1],  0.5 straight position
            float bankside = Mathf.InverseLerp(-90f,90f,rollAngle);
            //BankAmount Between[-1,1]
            float bankAmount = Mathf.Lerp(-1f, 1f, bankside);
      
            Vector3 bankTorque = bankAmount * rollSpeed * transform.up;
            _rb.AddTorque(bankTorque);

        }

        void HandleYaw() {
            //Rotate the airplane on the Y-Axis
            Vector3 yawTorque = _input.Yaw * yawSpeed * transform.up;
            _rb.AddTorque(yawTorque);


        }



        void HandleRigidbodyTransform()
        {   //Is the Rigidbody moving ? 
            if(_rb.velocity.magnitude > 1f)
            {
                Vector3 updatedVelocity = Vector3.Lerp(_rb.velocity,transform.forward * forwardSpeed,forwardSpeed*angleOfAttack*Time.deltaTime*rbLerpSpeed);
                _rb.velocity = updatedVelocity;

                Quaternion updatedRotation = Quaternion.Slerp(_rb.rotation,Quaternion.LookRotation(_rb.velocity.normalized,transform.up), Time.deltaTime *rbLerpSpeed);
               _rb.MoveRotation(updatedRotation);   
            }

        }

}

}