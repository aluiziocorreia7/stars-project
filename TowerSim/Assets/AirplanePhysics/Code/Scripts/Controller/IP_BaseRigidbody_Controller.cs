﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IndiePixel{

	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(AudioSource))]
	public class IP_BaseRigidbody_Controller : MonoBehaviour 
	{

		protected Rigidbody rb;
		protected AudioSource aSource;

		// Use this for initialization
		public virtual void Start () {
			rb = GetComponent<Rigidbody>();
			aSource = GetComponent<AudioSource>();

			if(aSource){
				aSource.playOnAwake = false;
			}
		
		}
	
		
		void FixedUpdate () {
			if(rb){
				HandlePhysics();
			}
		}


		protected virtual void HandlePhysics(){

		}
}
}