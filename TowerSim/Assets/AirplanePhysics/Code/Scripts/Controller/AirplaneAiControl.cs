using System;
using UnityEngine;
using IndiePixel;
using Random = UnityEngine.Random;


public enum AircraftState
{
    TAXING,
    HOLDING_POSITION,
    FLYING,
    PARKED
}

public class AirplaneAiControl : MonoBehaviour
    {
        // This script represents an AI 'pilot' capable of flying the plane towards a designated target.
        // It sends the equivalent of the inputs that a user would send to the Aeroplane controller.
        [SerializeField] private float m_RollSensitivity = .2f;         // How sensitively the AI applies the roll controls
        [SerializeField] private float m_PitchSensitivity = 1f;        // How sensitively the AI applies the pitch controls
        [SerializeField] private float m_YawSensitivity = .01f;        // How sensitively the AI applies the yaw controls
        [SerializeField] private float m_LateralWanderDistance = 5;     // The amount that the plane can wander by when heading for a target
        [SerializeField] private float m_LateralWanderSpeed = 0.11f;    // The speed at which the plane will wander laterally
        [SerializeField] private float m_MaxClimbAngle = 25;            // The maximum angle that the AI will attempt to make plane can climb at
        [SerializeField] private float m_MaxRollAngle = 45;             // The maximum angle that the AI will attempt to make plane roll
        [SerializeField] private float m_SteerAngle = 90f;              // The maximum angle that the AI will attempt to make plane steer
        [SerializeField] private float m_SpeedEffect = 0.01f;           // This increases the effect of the controls based on the plane's speed.
        [SerializeField] private float m_TakeoffHeight = 50f;            // the AI will fly straight and only pitch upwards until reaching this height
        [SerializeField] private float throttleInput = 0.1f;

        [SerializeField]  private Vector3 m_Target;                // the target to fly towards

    
        private Vector3[] _path;
        private float speedInput = 0.1f;
        private float m_RandomPerlin;                       // Used for generating random point on perlin noise so that the plane will wander off path slightly
        private bool m_TakenOff;                            // Has the plane taken off yet
        private IP_BaseAirplane_Input Input;
        private IP_Airplane_Characteristics characteristics;
        private IP_Airplane_Controller airplaneController;
        private float pitchInput = 0;
        private float yawInput = 0;
        private float rollInput = 0;
        public bool autoPilot = false;

        




    // setup script properties
    private void Awake()
    {

        Input = GetComponent<IP_BaseAirplane_Input>();
        characteristics = GetComponent<IP_Airplane_Characteristics>();
        airplaneController = GetComponent<IP_Airplane_Controller>();

        // pick a random perlin starting point for lateral wandering
        m_RandomPerlin = Random.Range(0f, 100f);


    }


  

    // fixed update is called in time with the physics system update
    private void FixedUpdate(){

        /*
        if (autoPilot)
        {
            Vector3 localTarget = transform.position + new Vector3(0, 0.5f, 5);
           // Debug.Log("LocalTarget: " + localTarget);
            Fly(localTarget);
        }*/

    }//FixedUpdate




    private void SetTakenOff()
    {
        if (airplaneController.CurrentMSL > m_TakeoffHeight)
        {
            m_TakenOff = true;
            Debug.Log("m_TakenOFF true");
        }
        else
            m_TakenOff = false;
    }


    public void EnableTakeOff()
    {
        m_TakenOff = true;
    }



       // Allows other scripts to set the plane's target
      public void SetTarget(Vector3 target)
        {
            m_Target = target;
            Debug.Log("Target was set");
        }

        
        public void SetAutoPilot(bool value)
          {
           autoPilot = value;
          }
    
        public void SetSpeedInput(float amount)
          {
            speedInput = amount;
            Debug.Log("Change Speed Input amount:" + speedInput);
           }
     


        public void ChangeAltitude(float amount)
         {
          m_Target.Set(m_Target.x, amount, m_Target.z);

        }

    public void SetPath (Vector3[] path)
    {
        _path = path;
        Debug.Log("SET PATH AICONTROLL");
    }

    public Vector3[] GetPath()
    {
        return _path;
    }


   


        //Fly the airplane
     public void Fly(Vector3 target)
        {
        //Debug.Log("FLYING TO TARGET");
        
        Vector3 localTarget;
        localTarget = transform.InverseTransformPoint(target);
        Debug.DrawRay(transform.position, localTarget, Color.blue);
        // Calculate the yaw and pitch towards the target
        float targetAnglePitch = -(localTarget.y / localTarget.magnitude) * m_MaxClimbAngle;
        float targetAngleYaw = (localTarget.x / localTarget.magnitude) * m_MaxRollAngle;
       
        //Adjusts the yaw, pitch and roll towards the target
        pitchInput = targetAnglePitch * m_PitchSensitivity;       
        yawInput = targetAngleYaw * m_YawSensitivity;
        rollInput = -(characteristics.rollAngle - targetAngleYaw) * m_RollSensitivity;

        // Pass the current inputs
        if (characteristics.mph >= characteristics.maxMPH)
            Input.AutoPilotInputs(pitchInput, rollInput, yawInput, 0);
        else
            Input.AutoPilotInputs(pitchInput, rollInput, yawInput, throttleInput);

      

     }//End


    public void TakeOff()
    {

        if (transform.position.y < m_TakeoffHeight)
        {
            Input.AutoPilotInputs(0, 0, 0, throttleInput);
            
        }
        else
        {
            autoPilot = true;
            this.GetComponent<Animator>().SetTrigger("fly");
         
        }

    }



    //Taxi the airplane
    public void Taxi(Vector3 target)
    {
        Vector3 localTarget;
        localTarget = transform.InverseTransformPoint(target);
        float steerInput = (localTarget.x / localTarget.magnitude) * m_SteerAngle;
        steerInput = steerInput * m_YawSensitivity;

       
        // Pass the current inputs
        if (characteristics.mph >= characteristics.maxTaxiSpeed)
            Input.AutoPilotInputs(0, 0, steerInput, -1);
        else
            Input.AutoPilotInputs(0, 0, steerInput, 0.05f);

        //print("MaxSpeed:" + characteristics.maxTaxiSpeed * speedInput);
    }//End


    //Taxi the airplane
    public void PushAndStart(Vector3 target)
    {
        /* Vector3 localTarget;
         localTarget = transform.InverseTransformPoint(target);
         float steerInput = (localTarget.x / localTarget.magnitude) * m_SteerAngle;
         steerInput = -steerInput * m_YawSensitivity;


         // Pass the current inputs
         if (characteristics.mph >= characteristics.maxTaxiSpeed)
             Input.AutoPilotInputs(0, 0, steerInput, 0);
         else */
        Input.AutoPilotInputs(0, 0, 0, -1f);
       



        
    }//End



    //Apply brakes to the wheels  {0 : 1} 
    public void ApplyBrakes(float amount, bool value)
    {
        Input.Brake = amount;
        GetComponent<IP_Airplane_Controller>().ShutEngineOff(value);
        
    }
    

}
