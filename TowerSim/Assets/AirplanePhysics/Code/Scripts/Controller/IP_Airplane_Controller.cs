﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace IndiePixel{

[RequireComponent(typeof(IP_Airplane_Characteristics))]
public class IP_Airplane_Controller : IP_BaseRigidbody_Controller {

	//Properties
	[Header("Base Airplane Properties")]
    public IP_Airplane_Preset airplanePreset;
	public IP_BaseAirplane_Input input;
	public IP_Airplane_Characteristics characteristics;
	public Transform centerOfGravity;

	[Tooltip("Weight is in LBS")]
	public float airplaneWeight = 800f;
       

    [Header("Engines")]
	public List<IP_Airplane_Engine> engines = new List<IP_Airplane_Engine>();

	[Header("Wheels Collider")]
	public List<IP_Airplane_Wheel> wheels = new List<IP_Airplane_Wheel>();

    [Header("Control Surfaces")]
    public List<IP_Airplane_ControlSurface> controlSurfaces = new List<IP_Airplane_ControlSurface>();

     //Properties     
    private float currentMSL;
    public float CurrentMSL
    {
        get { return currentMSL; }
    }


    private float currentAGL;
    public float CurrentAGL
    {
        get { return currentAGL; }
    }

	//Constants
    const float poundsToKilos = 0.453592f;
    const float metersToFeet = 3.28084f;


	public override void Start(){
        //Load Preset for the airplane
        GetPresetInfo(); 

		base.Start();
		//Convert the mass in Lbs to Kilos
		float finalMass = airplaneWeight * poundsToKilos;

		if(rb){
			//Set the converted mass into the rigidbody
			rb.mass = finalMass;
			if(centerOfGravity){
				//Set the center of gravity
				rb.centerOfMass = centerOfGravity.localPosition;
			}

			characteristics = GetComponent<IP_Airplane_Characteristics> ();
			if (characteristics) {
				characteristics.InitCharacteristics (rb,input);

			}

		}

		//Set all the wheels to the airplane
		if(wheels != null){
			if(wheels.Count > 0){
				foreach(IP_Airplane_Wheel wheel in wheels){
					wheel.InitWheel();
				}
			}
		}




	}


	protected override void HandlePhysics(){
		if(input){
			HandleEngines();
			HandleCharacteristics();
            HandleControlSurfaces();
            HandleWheel();
			HandleAltitude();
		}
	}


	void HandleEngines(){
		if(engines != null){
		if(engines.Count > 0){

			foreach(IP_Airplane_Engine engine in engines){
				//Add Force for all the airplane engines 
				rb.AddForce(engine.CalculateForce(input.StickyThrottle));
			}

		}
	}
}




	void HandleCharacteristics(){
		if (characteristics) {
			characteristics.UpdateCharacteristics ();
		}
	}


    void HandleControlSurfaces(){

        if(controlSurfaces.Count > 0)
        {
            foreach(IP_Airplane_ControlSurface controlSurface in controlSurfaces)
            {
                controlSurface.HandleControlSurface(input);

            }

        }

    }


    void HandleWheel(){

        if(wheels.Count>0)
        {
            foreach(IP_Airplane_Wheel wheel in wheels)
            {
                wheel.HandleWheel(input);
            }
        }
	}

		

	void HandleAltitude(){

        currentMSL = transform.position.y * metersToFeet;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down,out hit))
            {
                if(hit.transform.tag == "ground")
                {
                    currentAGL = (transform.position.y - hit.point.y) * metersToFeet;
                    
                }
            }
             
	}


        public void ShutEngineOff(bool value){
            foreach (IP_Airplane_Engine engine in engines)
            {
                //ShutOff the engine
                engine.ShutEngineOff = value;
            }

        }


    void GetPresetInfo()
    {

        if (airplanePreset)
        {
            airplaneWeight = airplanePreset.airplaneWeight;
            centerOfGravity.localPosition = airplanePreset.cogPosition;

            if (characteristics)
            {
                characteristics.dragFactor = airplanePreset.dragFactor;
                characteristics.flapDragFactor = airplanePreset.flapDragFactor;
                characteristics.liftCurve = airplanePreset.liftCurve;
                characteristics.maxLiftPower = airplanePreset.maxLiftPower;
                characteristics.maxMPH = airplanePreset.maxMPH;
                characteristics.rollSpeed = airplanePreset.rollSpeed;
                characteristics.yawSpeed = airplanePreset.yawSpeed;
                characteristics.pitchSpeed = airplanePreset.pitchSpeed;
                characteristics.rbLerpSpeed = airplanePreset.rbLerpSpeed;

            }
        }
    }



    }
}